;(function($, window, document, undefined) {
    var pluginName = 'clientstoggle',
        defaults = {
            propertyName: "value"
        };

    var Plugin = function(element, options){

        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    var hideAllInfo = function(){
        var info = $('.client-info');
        info.hide('normal');
    };

    var toggleInfo = function(el){
        var info = el.find('.client-info');
        var itemClients = $('.item-client');
        if(el.hasClass('active')){
            console.log('active');
            info.hide('normal');
            itemClients.removeClass('active');
        }else{
            console.log('not active');
            info.show('normal');
            itemClients.removeClass('active');
            el.addClass('active');
        }


    };

    Plugin.prototype.init = function () {
        var itemClients = $('.item-client');
        itemClients.each(function(ind, el){
            var el = $(el);
            var item = el.find('.item-box');
            item.on('click', function(){
                hideAllInfo();
                toggleInfo(el);
            });
        });
    };


    $.fn[pluginName] = function(options){
        return this.each(function(){
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
    /*------------------------ВЫЗОВ------------------------------------*/
    $(function(){
        $(document).clientstoggle();
    });
})(jQuery, window, document);







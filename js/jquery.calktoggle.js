;(function($, window, document, undefined) {
    var pluginName = 'calktoggle',
        defaults = {
            propertyName: "value"
        };

    var Plugin = function(element, options){

        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype.init = function () {
        var culkBtn = $('.culk-btn');
        culkBtn.on('click', function(){
            $('.calkulate-main-box').toggle('normal');
        });

    };


    $.fn[pluginName] = function(options){
        return this.each(function(){
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
    /*------------------------ВЫЗОВ------------------------------------*/
    $(function(){
        $(document).calktoggle();
    });
})(jQuery, window, document);







;(function($, window, document, undefined) {
    var pluginName = 'studiotoggle',
        defaults = {
            propertyName: "value"
        };

    var Plugin = function(element, options){

        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    var hideAllInfo = function(){
        var info = $('.studio-item-menu');
        info.stop().hide('normal');
    };

    var toggleInfo = function(el){
        var info = el.find('.studio-item-menu');
        var itemWork = $('.studio-item');
        if(el.hasClass('active')){
            info.stop().hide('normal');
            itemWork.removeClass('active');
        }else{
            info.stop().show('normal');
            itemWork.removeClass('active');
            el.addClass('active');
        }
    };

    Plugin.prototype.init = function () {
        var itemWork = $('.studio-item');
        itemWork.each(function(ind, el){
            var el = $(el);
            el.on('click', function(){
                hideAllInfo();
                toggleInfo(el);
            });
        });
    };


    $.fn[pluginName] = function(options){
        return this.each(function(){
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
    /*------------------------ВЫЗОВ------------------------------------*/
    $(function(){
        $(document).studiotoggle();
    });
})(jQuery, window, document);







;(function($, window, document, undefined) {
    var pluginName = 'workstoggle',
        defaults = {
            propertyName: "value"
        };

    var Plugin = function(element, options){

        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    var hideAllInfo = function(){
        var info = $('.work-item-menu');
        var grey = $('.grey');
        info.stop().hide('normal');
        grey.animate({'height': '100%'});
    };

    var toggleInfo = function(el){
        var info = el.find('.work-item-menu');
        var itemWork = $('.work-item');
        var grey = el.find('.grey');
        if(el.hasClass('active')){
            info.stop().hide('normal');
            grey.animate({'height': 'auto'});
            itemWork.removeClass('active');
        }else{
            info.stop().show('normal');
            itemWork.removeClass('active');
            grey.animate({'height': '0'});
            el.addClass('active');
        }
    };

    Plugin.prototype.init = function () {
        var itemWork = $('.work-item');
        itemWork.each(function(ind, el){
            var el = $(el);
            el.on('click', function(){
                hideAllInfo();
                toggleInfo(el);
            });
        });
    };


    $.fn[pluginName] = function(options){
        return this.each(function(){
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
    /*------------------------ВЫЗОВ------------------------------------*/
    $(function(){
        $(document).workstoggle();
    });
})(jQuery, window, document);






